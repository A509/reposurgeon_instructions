#############################################################################
## Install reposurgeon (if your distro doesn't have it in its package manager)

#http://www.catb.org/~esr/reposurgeon/INSTALL.html

#------------------------------------------------------------------------------
#Install the version of go that reposurgeon needs, if it's not in your distro's
#package repositories, as of Ubuntu 18.04, it isn't.
#
#https://github.com/golang/go/wiki/Ubuntu
#
#I don't know how important having the exact version of go is.
#
#Even though the docs say that golang-1.12 is enough, the latest version as 
#of 2/24/2020 needs go 1.13 (or at least one of its dependencies does).

# Don't try and install it from a PPA, as of today the 1.13 version of the PPA
# doesn't seem to work right, I got
# # gitlab.com/esr/reposurgeon/surgeon
# surgeon/reposurgeon.go:2301:24: constant 4294967295 overflows int
# surgeon/reposurgeon.go:2305:30: constant 4294967295 overflows int
# surgeon/selection.go:952:13: constant -9223372036854775808 overflows int

# instead download it from here https://golang.org/dl/
# and follow the instructions, including the part where they add
# go to path.

#If everything's working, you should be able to type "go" in some
#random directory and it should show command line information

# After this you can continue with the rest of the readme

# "make get" is as weird as hell to me as a command but it actually does work
# as long as go is installed

# then export the path to wherever you compiled reposurgeon to, maybe ~/reposurgeon/reposurgeon.

#############################################################################
## Now convert the eduke32 SVN to git

#in some directory of your choice (NOT in a checked out Eduke32 svn tree)
repotool initialize eduke32

#Note that "eduke32" passed into initialize is not a directory, it's just a name,
#it doesn't look like there's a way to have repotool operate on a directory.

#this will generate a Makefile, all of repotool's functionality happens
#through the Makefile

#--------------------------------------------------------------------------------
# Edit the Makefile

#change the value of REMOTE_URL to the URL of the SVN repository (online)
REMOTE_URL = https://svn.eduke32.com/eduke32/

# set READ_OPTIONS to use --user-ignores, so that the git repository will
# use .gitignore in the svn tree instead of using the svn ignore property
READ_OPTIONS = --user-ignores

# Put the SVN revision numbers in the commit messages.
#
# from https://kartaca.com/en/migrating-codebases-from-svn-to-git/
# in the "Subversion to Git Migration" section

# in the Makefile, change the eduke32.fi line to something like this:
# Build the second-stage fast-import stream from the first-stage stream dump
# [75] note that an "event" isn't a commit, and the $ operator doesn't seem to work to mean "the last event" so I just set the upper bound to 9999999 then let it fail and tell me what the last index was
```
eduke32.fi: eduke32.svn eduke32.opts eduke32.lift eduke32.map $(EXTRAS)
        $(REPOSURGEON) $(VERBOSITY) "script eduke32.opts" "read $(READ_OPTIONS) <eduke32.svn" "authors read <eduke32.map" "sourcetype svn" "prefer git" "script eduke32.lift" '1..$$ append --legacy From SVN:\ r\%LEGACY\%' "legacy write --le$
```
# you will need the latest version of reposurgeon for this from git, only the latest version supports %LEGACY% appends to the commit message.

#--------------------------------------------------------------------------------
# Set eduke32.map (the user information file)

# Using https://gitlab.com/esr/reposurgeon/issues/114
# as "instructions" (yeah, that's honestly the best I could find!)

# Then create a new file named eduke32.map,
# TODO: Does reposurgeon create an empty file for this on a clean run?
# (or edit the existing file), and replace all of its contents with this:

# contents of eduke32.map:
terminx = Richard Gobeille <terminx@gmail.com>
hendricks266 = Evan Ramos <hendricks266@gmail.com>
pogokeen = Alex Dawson <pogokeen@gmail.com>
seventyfive = jwaffe <jwaffe>
helixhorned = Philipp Kutin <helixhorned@gmail.com>
Plagman = Pierre-Loup A. Griffais <git@plagman.net>
plagman = Pierre-Loup A. Griffais <git@plagman.net>
qbix79 = Peter Veenstra <qbix79>
hnt_ts = Hunter_rus <hunter_byte@mail.ru>
ny00123 = NY00123 <ny00@outlook.com>
striker = Jordon Moss <mossj32@gmail.com>

#--------------------------------------------------------------------------------
# Edit eduke32.opts to specify which branch should be "master" for git

# then https://gitlab.com/esr/reposurgeon/issues/114#note_87775243
# edit eduke32.opts and add something like this
# branchify project1/trunk project1/tags/* project1/branches/*
# it's hard to figure out what exactly is Eduke32's branch, I took a guess...
# seemed to work.

branchify eduke32/eduke32

# NOTE: don't even try and do 
#   $ reposurgeon read <eduke32.fi,
# it will crash with 
# $ reposurgeon
# reposurgeon% read <eduke32.fi --user-ignores
# reposurgeon: from eduke32.fi...8627 svn commits...(13.10 sec) done.
# 2020-02-18T02:49:28Z * eduke32
# Traceback (most recent call last):
#  File "/usr/bin/reposurgeon", line 12373, in <module>
#    main()
#  File "/usr/bin/reposurgeon", line 12349, in main
#    interactive()
#  File "/usr/bin/reposurgeon", line 12338, in interactive
#    interpreter.cmdloop()
#  File "/usr/lib/python2.7/cmd.py", line 143, in cmdloop
#    stop = self.postcmd(stop, line)
#  File "/usr/bin/reposurgeon", line 7913, in postcmd
#    assert unused is not None   # pacify pylint
#AssertionError

#############################################################################
# Generate the git repository

make

#############################################################################
# Test the git repository

# Then to test, I pushed it to gitlab,
# cd into the eduke32-git directory, then
git remote add origin https://gitlab.com/A509/reposurgeontest4.git
git push -u origin --all
git push -u origin --tags